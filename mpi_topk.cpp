// example implementation of parallel partial sorting using user-defined MPI reduction operation.
//
// each MPI rank starts with 10 random numbers in 'vec_local' and
// after sorting, global top-5 items are saved to 'vec_top' on rank=0

#include <algorithm> //partial_sort
#include <cstdlib>   //rand
#include <iostream>  //cout
#include <mpi.h>
#include <vector> //vector

// this defines if "i" should go before the "j" in sorting
bool my_sorter(double i, double j) { return (i > j); }

// the function signature is defined by the
// MPI standard (for example: v3.1 §5.9.5 User-Defined Reduction Operations)
void select_top_5(void *input_list, void *output_list, int *length,
                  MPI_Datatype *datatype) {
  const int top_items_count = 5;

  for (int i = 0; i < *length; ++i) {
    std::vector<double> merged_list;
    const int offset_start = i * top_items_count;
    const int offset_end = offset_start + top_items_count;

    merged_list.insert(merged_list.end(),
                       static_cast<double *>(input_list) + offset_start,
                       static_cast<double *>(input_list) + offset_end);
    merged_list.insert(merged_list.end(),
                       static_cast<double *>(output_list) + offset_start,
                       static_cast<double *>(output_list) + offset_end);

    std::partial_sort_copy(merged_list.begin(), merged_list.end(),
                           static_cast<double *>(output_list) + offset_start,
                           static_cast<double *>(output_list) + offset_end,
                           my_sorter);
  }
}

int main(int argc, char **argv) {

  MPI_Init(NULL, NULL);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  int my_world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_world_rank);

  const int total_items_count = 10;
  const int top_items_count = 5;

  srand(my_world_rank + 1); // seed each rank's random-number generator differently

  MPI_Datatype pentad;
  MPI_Type_contiguous(top_items_count, MPI_DOUBLE, &pentad);
  MPI_Type_commit(&pentad);

  MPI_Op top5;
  // register the "top5" operation as commutative and associative
  MPI_Op_create(*select_top_5, 1, &top5);

  std::vector<double> vec_local(total_items_count);
  std::vector<double> vec_top(top_items_count);

  for (auto &element : vec_local) {
    element = rand() % 100;
  }

  for (int rank = 0; rank < world_size; ++rank) {
    if (rank == my_world_rank) {
      std::cout << "rank " << rank << "\'s items: ";
      for (auto item : vec_local)
        std::cout << item << " ";
      std::cout << "\n";
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  // sort top 5 locally
  std::partial_sort(vec_local.begin(), vec_local.begin() + top_items_count,
                    vec_local.end(), my_sorter);

  // sort top 5 globally
  MPI_Reduce(vec_local.data(), vec_top.data(), 1, pentad, top5, 0,
             MPI_COMM_WORLD);

  std::cout.flush();
  if (my_world_rank == 0) {
    std::cout << "global top-"<< top_items_count << " items: ";
    for (auto element : vec_top)
      std::cout << element << " ";
    std::cout << "\n";
  }

  MPI_Finalize();
}
