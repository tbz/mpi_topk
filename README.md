Sample implementation of the distributed top-k sorting in C++ based on `MPI_reduce` with a user-defined reduction operator.

This code was originally developed for [
cpp_sisso](https://gitlab.mpcdf.mpg.de/nomad-lab/cpp_sisso) project.
